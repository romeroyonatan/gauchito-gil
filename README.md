# Gauchito GIL

Charla PyconAR 2018 - Concurrencia en Python

> Entendiendo el GIL (Global Interpreter Lock) 

Programar aplicaciones concurrentes en Python tiene particularidades que en
otros lenguajes no existen. Se intentará explicar las limitaciones de Python
para tareas concurrentes, qué rol cumple el GIL y cuales son las formas de
sortear estas limitaciones usando multihilo, multiproceso y asincronismo
¿Cuando conviene cada una? 

Como caso práctico se presentarán versiones multiproceso, multihilo y
asincrónica de una mini aplicación que consume una API REST.


## Guion

En esta charla no voy a hablar de la leyenda gauchito gil, tal como lo conoce
la gente de Corrientes, sino otra version que cuenta su labor dentro del
interprete de python

Como todos sabemos Guido Van Rossum es un devoto del gauchito gil y no quiso
dejarlo afuera de python.

Pero primero, antes que nada quiero que repasemos algunos conceptos sobre
concurrencia y paralelismo para que todos entendamos mas o menos de lo que se
trata esta charla. Ademas vamos a hablar de hilos y procesos y sobre la
diferencia entre procesos CPU-bound o IO-bound.


### CONCURRENCIA

La concurrencia es la composicion de procesos ejecutados de forma
independiente, dicho de otra forma es manejar un monton de cosas que pasan al
mismo tiempo. 

Por ejemplo, Homero esta comiendo de distintas bolsas de forma concurrente, de
a una por vez.


### PARALELISMO

El paralelismo es la ejecucion simultanea de estos procesos, es decir hacer un
monton de cosas al mismo tiempo.

Por ejemplo, Los perritos estan comiendo todos al mismo tiempo, de forma
paralela

Parece que paralelismo y concurrencia es lo mismo, y tiene sentido, porque un
tarea que puedo ejecutar de forma paralela, debe ser concurrente.


### HILOS Y PROCESOS

Para implementar la concurrencia en una computadora se utilizan los procesos y
los hilos.

Los procesos sos administrados por el sistema operativo. Ademas del codigo
ejecutable contienen un monton de datos adicionales duplicados. Cada uno tiene
su contexto, su seccion de memoria y una copia de todas las variables al
momento de crear cada proceso. En general los procesos son pesados.

Los hilos por otro lado, son tambien llamados procesos livianos. Los hilos
viven dentro de un proceso y comparten las variables y el contexto del mismo.
Solo contienen una pequeño contexto necesario para operar.

La teoria nos indica que los hilos son mas livianos que los procesos


### TIPOS DE PROCESOS

Los procesos ademas se pueden clasificar como CPU-bound o IO-bound.

Los procesos CPU-bound se caracterizan por hacer muchos calculos y utilizar
mucho la CPU. Por ejemplo los programas cientificos, los de criptografia,
renderizar videos, juegos, etc.

Los procesos IO-bound son aquellos que hacer uso de recursos de entrada y
salida, como puede ser el disco o mas comunmente la red. Por ejemplo un
servidor web o los sistemas ABM (alta, baja, modificacion).


### EJEMPLO CPU-BOUND

Esta es la funcion mas simple que podemos hacer CPU-bound. Como vemos lo unico
que es es contar hacia atras la cantidad de veces que le pasemos por parametro.

Vamos a ejecutarla de forma secuencial, muchas veces, unas 150 millones de
veces. Como vemos tarda mas o menos 4 segundos

Si una tarea que tiene que hacer una persona tarda un determinado tiempo, si la
hacen dos deberian tardar mas o menos la mitad, no?

Como puedo ejecutar esa funcion de forma concurrente, vamos a dividir el
trabajo entre los 4 nucleos que tiene mi CPU. Voy a crear 4 procesos y dividir
el trabajo para que cada uno haga un cuarto del trabajo. Deberia tardar menos
tiempo no?

(mostrar y ejecutar multiproceso.py) ...

Bueno, tardo mas o menos la mitad del tiempo que antes, que no esta mal, pero
como tengo 4 CPU esperaba que lo haga en un cuarto del tiempo.

Como dijimos antes los hilos son mas livianos que los procesos, asi que vamos a
hacerlo con hilos y esperemos que mas o menos tarde 1 segundo (4 segundos
dividido los 4 nucleos).

(mostrar y ejecutar multihilo.py) ...

### KRUSTY

Tardo el doble de tiempo de lo que tarda hacerlo en forma secuencial. Que paso?
Por que tardo tanto? 


### EL CULPABLE

Quien es el culpable de esto? El GIL o Global Interpreter Lock

### GIL

El GIL es un bloqueo que no permite ejecutar codigo python en forma simultanea

### PORQUE EXISTE El GIL?

La existencia del se debe al origen de Python, ya que el interprete CPython no
se diseño para trabajar con multiples hilos, entonces pueden haber problemas
como condiciones de carrera o errores aleatorios.

### Y SI SACAMOS EL GIL Y HACEMOS PYTHON MAS RAPIDO?

No es tan facil sacar el GIL, porque muchas features del lenguaje crecieron
asumiendo la existencia del GIL, y citando el ZEN de Python, los casos
especiales no son lo suficientemente especial para romper las reglas, aunque la
practicidad derrota a la pureza.

### EL GIL EN OTRAS IMPLEMENTACIONES

El GIL no existe en todas las implementaciones de Python, en Jython y en
IronPython por ejemplo no existe el GIL ya que los hilos no son manejados por
el sistema operativo, sino que los administra la maquina virtual de Java y .Net
respectivamente.

En Pypy, que es un interprete de python escrito en python, sí existe el GIL
como en CPython.

Y por ultimo, en Cython tambien existe el GIL, pero se puede liberar con un
bloque with

### TENEMOS QUE APRENDER A CONVIVIR CON EL GIL

Vamos a tener que aprender a convivir con el GIL, para eso tenemos que
conocerlo

En el ejemplo del principio les hice trampa, porque les mostre un caso de borde
para que resalte la diferencia de tiempos entre multiproceso y multihilo.

El GIL se libera con una llamada de IO, como puede ser esperar recibir bytes de
la red. En ese momento el hilo que estaba en ejecucion se bloquea y el
interprete elije otro hilo que esta esperando para ejecutar y lo pone a
ejecutar hasta que termine o haga una llamada de IO.


### ¿COMO LIBERAR EL BLOQUEO?
El GIL se puede liberar mediante llamadas al sistema operativo, como por
ejemplo descargar un archivo de Internet, o creando una extension en C
y llamar a PyEvel_SaveThread



### TIPS PARA TRABAJAR CON CONCURRENCIA EN PYTHON

La gente no entiende la diferencia sobre cuando usar multiproceso o multihilo,
pero Ned tiene un truquillo para recordarlo:


> Si los calculos no son sencillos, hazlo multiprocesillo  
> Si hay que esperar un monton, es multihilos muchachon


En otras palabras, lo que Ned nos quiere decir es que si nuestra aplicacion es
CPU-bound usemos multiprocesos, en cambio si es IO-bound usemos multihilos


### ASINCRONISMO

Existe otra forma de trabajar con concurrencia en procesos IO-bound que es el
asincronismo, que tiene como ventaja sobre los hilos que es mucho mas liviano y
como solo se ejecuta un hilo, no hay que preocuparse por las condiciones de
carrera

No voy a profundizar mucho sobre asincronismo por el tiempo, pero en resumen es
un programa que trabaja en un hilo, donde las tareas cooperativamente liberan
el event loop para que otra tarea se ejecute. El event loop es quien coordina
la tarea que se va a ejecutar.

La desventaja es que al ser cooperativo, si una tarea no libera el event loop,
ninguna otra tarea va a progresar, por lo tanto hay que tener mucho cuidado al
programar tareas asincronicas

### EJEMPLO IO-BOUND

Vamos a hacer un programita para descargar memes de los simpsons desde un
servidor web.

Primero veamos el ejemplo multiproceso

*(Mostrar codigo multiproceso y ejecutarlo, medir tiempos)*

Ahora vamos a verlo en multihilos. Fijense que gracias al buen diseño del
modulo `concurrent.futures`, con solamente cambiar unas lineas el codigo
funciona perfectamente.

*(Mostrar codigo multihilos y ejecutarlo, medir tiempos)*

Para terminar voy a mostrar el mismo codigo en forma asincronica, fijense que
si bien hay diferencias con el codigo de `concurrent.futures`, la API es
parecida.

*(Mostrar codigo multihilos y ejecutarlo, medir tiempos)*

### EXECUTORS

Una de las desventajas del codigo asincronico es que las librerias que usemos
deben ser asincronicas tambien para no bloquear el Event loop.

Es posible que si queremos migrar a asincronico, muchas de las librerias no
sean asincronicas. Para sortear este obstaculo, podemos ejecutar una callable
dentro de otro hilo que se comporta como si fuera una corutina y no bloquea el
loop principal.

*(Mostrar ejemplo executors)*

Los ejecutores pueden ejecutarse multihilo (por defecto) o pueden ser
multiproceso, util para CPU-bound.


### CONCLUSION

En esta charla vimos que es el GIL, por que existe en Python y los problemas
que nos pueden ocasionar. Tambien vimos una receta para saber si conviene usar
multiproceso y multihilo segun el problema que querramos resolver.

En resumen, si nuestro proceso es CPU-bound conviene hacer multiproceso porque
el GIL no es liberado al no haber llamadas de IO. Si el proceso es IO-bound
conviene hacer multihilos o asincronismo, ya que los hilos son mucho mas
livianos que los procesos

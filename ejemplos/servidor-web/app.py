import itertools
import os

import responder  # fades


api = responder.API()


def memes_generator():
    """Obtiene un generador de nombres de archivos estaticos"""
    for _, _, filenames in os.walk(api.static_dir):
        yield from (filename for filename in filenames)


memes = itertools.cycle(memes_generator())


@api.route("/")
def get_meme(req, resp):
    meme = next(memes)
    resp.media = {"href": f"{api.static_route}/{meme}"}


if __name__ == "__main__":
    api.run()

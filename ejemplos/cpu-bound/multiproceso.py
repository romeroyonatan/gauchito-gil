from multiprocessing import Process
import time


def cpu_bound(repeticiones):
    while repeticiones > 0:
        repeticiones -= 1


REPETICIONES = 150000000

start = time.time()


p1 = Process(target=cpu_bound, args=(REPETICIONES//4,))
p2 = Process(target=cpu_bound, args=(REPETICIONES//4,))
p3 = Process(target=cpu_bound, args=(REPETICIONES//4,))
p4 = Process(target=cpu_bound, args=(REPETICIONES//4,))

p1.start()
p2.start()
p3.start()
p4.start()

p1.join()
p2.join()
p3.join()
p4.join()

duracion = time.time() - start
print("Tiempo transcurrido {duracion:.2f} segundos".format(duracion=duracion))

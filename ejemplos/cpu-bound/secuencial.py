import time


def cpu_bound(repeticiones):
    while repeticiones > 0:
        repeticiones -= 1


REPETICIONES = 150000000

start = time.time()

cpu_bound(REPETICIONES)

duracion = time.time() - start
print("Tiempo transcurrido {duracion:.2f} segundos".format(duracion=duracion))

from threading import Thread
import time


def cpu_bound(repeticiones):
    while repeticiones > 0:
        repeticiones -= 1


REPETICIONES = 150000000

start = time.time()


t1 = Thread(target=cpu_bound, args=(REPETICIONES//4,))
t2 = Thread(target=cpu_bound, args=(REPETICIONES//4,))
t3 = Thread(target=cpu_bound, args=(REPETICIONES//4,))
t4 = Thread(target=cpu_bound, args=(REPETICIONES//4,))


t1.start()
t2.start()
t3.start()
t4.start()

t1.join()
t2.join()
t3.join()
t4.join()

duracion = time.time() - start
print("Tiempo transcurrido {duracion:.2f} segundos".format(duracion=duracion))

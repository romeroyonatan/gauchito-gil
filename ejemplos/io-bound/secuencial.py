import urllib.parse
import pathlib
import os
import time

import requests

CANTIDAD_MEMES = 10
OUTPUT_DIR = "/tmp/gauchito-gil"
ENDPOINT = "http://localhost:8000"


def get_meme_url():
    """Devuelve una URL para descargar un gif de gatitos

    La URL es solicitada a la API
    """
    response = requests.get(ENDPOINT)
    data = response.json()
    return urllib.parse.urljoin(ENDPOINT, data["href"])


def download_file(url, download_dir):
    """Descarga archivo y lo guarda en el disco"""
    response = requests.get(url)
    # Obtengo el nombre de archivo
    path = urllib.parse.urlparse(url).path
    _, filename = os.path.split(path)
    filename = os.path.join(download_dir, filename)
    # Guardo el contenido al archivo
    print("Guardando archivo " + filename)
    with open(filename, "wb") as f:
        f.write(response.content)


def download_one():
    """Descarga una imagen"""
    url = get_meme_url()
    print("Descargando " + url)
    download_file(url, OUTPUT_DIR)


def secuencial():
    """Descarga muchas imagenes de forma secuencial"""
    # Creo el directorio de descarga
    output_dir = pathlib.Path(OUTPUT_DIR)
    output_dir.mkdir(exist_ok=True)
    for _ in range(CANTIDAD_MEMES):
        download_one()


def main(download):
    inicio = time.time()
    download()
    duracion = time.time() - inicio
    print(f"Tiempo transcurrido: {duracion:.2f} segundos")


if __name__ == "__main__":
    main(secuencial)

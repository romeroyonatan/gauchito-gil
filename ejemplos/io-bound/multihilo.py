import concurrent.futures

from secuencial import CANTIDAD_MEMES, download_one, main


def multihilo():
    """Descarga muchas imagenes en paralelo usando hilos"""
    with concurrent.futures.ThreadPoolExecutor(max_workers=CANTIDAD_MEMES) as executor:
        futures = [executor.submit(download_one) for _ in range(CANTIDAD_MEMES)]
        concurrent.futures.wait(futures)


if __name__ == "__main__":
    main(multihilo)

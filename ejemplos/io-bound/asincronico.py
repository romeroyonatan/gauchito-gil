import asyncio
import os
import pathlib
import time
import urllib.parse

import aiohttp

from secuencial import CANTIDAD_MEMES

CANTIDAD_MEMES = 10
OUTPUT_DIR = "/tmp/gauchito-gil"
ENDPOINT = "http://localhost:8000"


async def get_meme_url():
    """Devuelve una URL para descargar un meme de los simpsons

    La URL es solicitada a la API
    """
    async with aiohttp.ClientSession() as session:
        async with session.get(ENDPOINT) as response:
            data = await response.json()
    return urllib.parse.urljoin(ENDPOINT, data["href"])


async def download_file(url, download_dir):
    """Descarga archivo y lo guarda en el disco"""
    # Obtengo el nombre de archivo
    path = urllib.parse.urlparse(url).path
    _, filename = os.path.split(path)
    filename = os.path.join(download_dir, filename)
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            # Guardo el contenido al archivo
            print("Guardando archivo " + filename)
            with open(filename, "wb") as f:
                f.write(await response.read())


async def download_one():
    """Descarga una imagen y la guarda en disco"""
    url = await get_meme_url()
    print("Descargando " + url)
    await download_file(url, OUTPUT_DIR)


async def asincronico():
    """Descarga muchas imagenes de forma asincronica"""
    output_dir = pathlib.Path(OUTPUT_DIR)
    output_dir.mkdir(exist_ok=True)
    futures = [download_one() for _ in range(CANTIDAD_MEMES)]
    await asyncio.wait(futures)


async def main(download):
    """Ejecuta el script y mide tiempos"""
    inicio = time.time()
    await download()
    duracion = time.time() - inicio
    print(f"Tiempo transcurrido: {duracion:.2f} segundos")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    coro = main(asincronico)
    loop.run_until_complete(coro)

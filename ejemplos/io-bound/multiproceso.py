import concurrent.futures

from secuencial import CANTIDAD_MEMES, download_one, main


def multiproceso():
    """Descarga muchas imagenes en paralelo usando procesos"""
    with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
        futures = [executor.submit(download_one) for _ in range(CANTIDAD_MEMES)]
        concurrent.futures.wait(futures)


if __name__ == "__main__":
    main(multiproceso)
